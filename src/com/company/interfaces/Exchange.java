package com.company.interfaces;

public interface Exchange {
    final int FLAG_USD = 1;
    final int FLAG_EURO = 2;
    double convert(int valueMoney, int flagValuta);
}
