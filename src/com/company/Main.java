package com.company;

import com.company.model.Bank;

import java.util.ArrayList;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        ArrayList<Organisation> organisations = Generate.getOrganisation();
        FinanceSystem financeSystem = new FinanceSystem(organisations);
        int valueMoneyForExchange = 20000;
        int valueForSend = 1000;

        financeSystem.print();
        Scanner in = new Scanner(System.in);

        System.out.println("Change UAH to:");
        System.out.println("Change valuta(USD - 1, EURO - 2):");
        int valueChange = in.nextInt();
        financeSystem.printBestExchange(valueMoneyForExchange, valueChange);

        financeSystem.printBestOrganisationForSend(valueForSend);


    }
}
