package com.company;

import com.company.model.Bank;
import com.company.model.CreditOrganisation;
import com.company.model.Obmenka;
import com.company.model.Pochta;

import java.util.ArrayList;

public class Generate {


    private Generate() {
    }

    public static ArrayList<Organisation> getOrganisation(){
        ArrayList<Organisation> organisations = new ArrayList<>();
        Organisation bank = new Bank("Privat", "Gagarina str.");
        Organisation pochta = new Pochta("Pochta", "Otdelenie nomer 1");
        Organisation exchange = new Obmenka("Obmenka", "First str.");
        Organisation lombard = new CreditOrganisation("Lombard", "Second str.", 50000, 40);
        Organisation creditCafe = new CreditOrganisation("CreditCafe", "Sumscaya str.", 4000, 200);
        Organisation creditSouse = new CreditOrganisation("CreditSouse", "Morozova str.", 100000, 20);
        organisations.add(bank);
        organisations.add(exchange);
        organisations.add(lombard);
        organisations.add(creditCafe);
        organisations.add(creditSouse);
        return organisations;
    }



}
