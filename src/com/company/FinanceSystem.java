package com.company;

import com.company.interfaces.Exchange;
import com.company.interfaces.SendMoney;

import java.util.ArrayList;

public class FinanceSystem {
    private ArrayList<Organisation> organisations;


    public FinanceSystem(ArrayList<Organisation> organisations) {
        this.organisations = organisations;
    }

    public void print(){
        for (Organisation organisation : organisations) {
            System.out.println(organisation.name);
        }
    }

    public void  printBestExchange(int valueMoneyForExchange, int flagValuta){
        Organisation bestOrganisationForExchange = null;
        double bestValue = 0;
        for (Organisation organisation : organisations) {
            if(organisation instanceof Exchange){
                double value = ((Exchange) organisation).convert(valueMoneyForExchange, flagValuta);
                if(bestValue == 0 || bestValue < value){
                    bestOrganisationForExchange = organisation;
                    bestValue = value;
                }
            }
        }
        if(bestOrganisationForExchange != null){
            System.out.println(String.format("Best exchanger %s return %.2f", bestOrganisationForExchange.getName(), bestValue));
        }else{
            System.out.println("not have best exchangeOrganisation");
        }
    }

    public void printBestOrganisationForSend(int valueUAH){
        Organisation bestOrganisationForSend = null;
        double bestValue = 0;
        for (Organisation organisation : organisations) {
            if(organisation instanceof SendMoney){
                double value = ((SendMoney) organisation).convertMoneyForSend(valueUAH);
                if(bestValue == 0 || bestValue < value){
                    bestOrganisationForSend = organisation;
                    bestValue = value;
                }
            }
        }
        if(bestOrganisationForSend != null){
            System.out.println(String.format("Best sender %s return %.2f", bestOrganisationForSend.getName(), bestValue));
        }else{
            System.out.println("not have best sendOrganisation");
        }
    }



}
