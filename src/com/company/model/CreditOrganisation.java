package com.company.model;

import com.company.Organisation;
import com.company.interfaces.GiveMoney;

public class CreditOrganisation extends Organisation implements GiveMoney {

    private int limitGiveMoney;
    private int percentGiveMoney;

    public CreditOrganisation(String name, String adress, int limitGiveMoney, int percentGiveMoney) {
        super(name, adress);
        this.limitGiveMoney = limitGiveMoney;
        this.percentGiveMoney = percentGiveMoney;
    }

    @Override
    public double convertGiveMoney(int valueUAH) {
        return valueUAH + ((valueUAH*percentGiveMoney)/valueUAH);
    }
}
