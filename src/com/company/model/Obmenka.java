package com.company.model;

import com.company.Organisation;
import com.company.interfaces.Exchange;

public class Obmenka extends Organisation implements Exchange{

    private int courceUSD = 28;
    private int courceEURO = 29;

    public Obmenka(String name, String adress) {
        super(name, adress);
    }

    @Override
    public double convert(int valueMoney, int flagValuta) {
        if(flagValuta == FLAG_USD){
            return valueMoney/courceUSD;
        }else{
            return valueMoney/courceEURO;
        }
    }
}
