package com.company.model;

import com.company.Organisation;
import com.company.interfaces.SendMoney;

public class Pochta extends Organisation implements SendMoney {


    public Pochta(String name, String adress) {
        super(name, adress);
    }

    @Override
    public double convertMoneyForSend(int valueUAH) {
        return valueUAH + (100*0.2);
    }
}
