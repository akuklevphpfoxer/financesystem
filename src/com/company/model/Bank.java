package com.company.model;

import com.company.Organisation;
import com.company.interfaces.Exchange;
import com.company.interfaces.SendMoney;

public class Bank extends Organisation implements Exchange, SendMoney{

    private int courceUSD = 26;
    private int courceEURO = 30;
    private int commission = 15;
    private int commisisonSend = 20;
    public Bank(String name, String adress)  {
        super(name, adress);
    }

    @Override
    public double convert(int valueMoney, int flagValuta) {
        if(flagValuta == FLAG_USD){
            return valueMoney/(courceUSD + commission/26);
        }else{
            return valueMoney/(courceEURO + commission/30);
        }
    }

    @Override
    public double convertMoneyForSend(int valueUAH) {
        return (valueUAH + (100*0.1)) + commisisonSend;
    }
}
